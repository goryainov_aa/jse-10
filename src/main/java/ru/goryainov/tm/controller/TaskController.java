package ru.goryainov.tm.controller;

import ru.goryainov.tm.repository.TaskRepository;
import ru.goryainov.tm.entity.Task;
import ru.goryainov.tm.service.ProjectTaskService;
import ru.goryainov.tm.service.TaskService;

import java.util.List;

public class TaskController extends AbstractController{

    private final TaskService taskService;

    private final ProjectTaskService projectTaskService;

    public TaskController(TaskService taskService, ProjectTaskService projectTaskService) {
        this.taskService = taskService;
        this.projectTaskService = projectTaskService;
    }

    /**
     * Вывод создания задачи
     */
    public int createTask() {
        System.out.println("[Create task]");
        System.out.println("[Please, enter task name:]");
        final String name = scanner.nextLine();
        taskService.create(name);
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Изменение задачи по индексу
     */
    public int updateTaskByIndex() {
        System.out.println("[Update task]");
        System.out.println("[Please, enter task index:]");
        final int index = Integer.parseInt(scanner.nextLine()) - 1;
        final Task task = taskService.findByIndex(index);
        if (task == null) {
            System.out.println("[Fail]");
            return 0;
        }
        System.out.println("[Please, enter task name:]");
        final String name = scanner.nextLine();
        System.out.println("[Please, enter task description:]");
        final String description = scanner.nextLine();
        taskService.update(task.getId(), name, description);
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Изменение задачи по идентификатору
     */
    public int updateTaskById() {
        System.out.println("[Update task]");
        System.out.println("[Please, enter task id:]");
        final long id = Long.parseLong(scanner.nextLine());
        final Task task = taskService.findById(id);
        if (task == null) {
            System.out.println("[Fail]");
            return 0;
        }
        System.out.println("[Please, enter task name:]");
        final String name = scanner.nextLine();
        System.out.println("[Please, enter task description:]");
        final String description = scanner.nextLine();
        taskService.update(task.getId(), name, description);
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Очистка задачи
     */
    public int clearTask() {
        System.out.println("[Clear task]");
        taskService.clear();
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Удаление задачи из списка по имени
     */
    public int removeTaskByName() {
        System.out.println("[Remove task by name]");
        System.out.println("[Please, enter task name:]");
        final String name = scanner.nextLine();
        final Task task = taskService.removeByName(name);
        if (task == null) System.out.println("[Fail]");
        else System.out.println("[Ok]");
        return 0;
    }

    /**
     * Удаление задачи из списка по идентификатору
     */
    public int removeTaskById() {
        System.out.println("[Remove task by id]");
        System.out.println("[Please, enter task id:]");
        final long id = scanner.nextLong();
        final Task task = taskService.removeById(id);
        if (task == null) System.out.println("[Fail]");
        else System.out.println("[Ok]");
        return 0;
    }

    /**
     * Удаление задачи из списка по индексу
     */
    public int removeTaskByIndex() {
        System.out.println("[Remove task by index]");
        System.out.println("[Please, enter task index:]");
        final int index = scanner.nextInt() - 1;
        final Task task = taskService.removeByIndex(index);
        if (task == null) System.out.println("[Fail]");
        else System.out.println("[Ok]");
        return 0;
    }

    /**
     * Просмотр задачи по индексу
     */
    public int viewTaskByIndex() {
        System.out.println("Enter, task index: ");
        final int index = scanner.nextInt() - 1;
        final Task task = taskService.findByIndex(index);
        viewTask(task);
        return 0;
    }

    /**
     * Просмотр задачи по идентификатору
     */
    public int viewTaskById() {
        System.out.println("Enter, task id: ");
        final long id = scanner.nextLong();
        final Task task = taskService.findById(id);
        viewTask(task);
        return 0;
    }

    /**
     * Просмотр списка задач
     */
    public void viewTask(final Task task) {
        if (task == null) return;
        System.out.println("[View task]");
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
        System.out.println("[Ok]");
    }

    /**
     * Вывод списка задач
     */
    public int listTask() {
        System.out.println("[List task]");
        viewTasks(taskService.findAll());
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Вывод списка задач
     */
    public void viewTasks(final List<Task> tasks){
        if (tasks == null|| tasks.isEmpty()) return;
        int index = 1;
        for (final Task task : tasks) {
            System.out.println(index + ". " + task.getId() + ": " + task.getName());
            index++;
        }
    }

    /**
     * Вывод списка задач в проекте по id
     */
    public int listTaskByProjectId(){
        System.out.println("[List task by project]");
        System.out.println("[Please, enter project id:]");
        final long projectId = Long.parseLong(scanner.nextLine());
        final List<Task> tasks = taskService.findAllByProjectId(projectId);
        viewTasks(tasks);
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Добавление задачи в проект по id
     */
    public int addTaskToProjectByIds(){
        System.out.println("[Add task to project by ids]");
        System.out.println("[Please, enter project id:]");
        final long projectId = Long.parseLong(scanner.nextLine());
        System.out.println("[Please, enter task id:]");
        final long taskId = Long.parseLong(scanner.nextLine());
        projectTaskService.addTaskToProject(projectId, taskId);
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Удаление задачи из проект по id
     */
    public int removeTaskToProjectByIds(){
        System.out.println("[Remove task from project by ids]");
        System.out.println("[Please, enter project id:]");
        final long projectId = Long.parseLong(scanner.nextLine());
        System.out.println("[Please, enter task id:]");
        final long taskId = Long.parseLong(scanner.nextLine());
        projectTaskService.removeTaskFromProject(projectId, taskId);
        System.out.println("[Ok]");
        return 0;
    }

}
